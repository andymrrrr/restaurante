import {ClienteLayouts}  from "../layouts"
import {home} from "../pages/Clients"
const rutasClientes = [
    {
        path:'/',
        layout: ClienteLayouts,
        component: home
    }
]

export default rutasClientes;
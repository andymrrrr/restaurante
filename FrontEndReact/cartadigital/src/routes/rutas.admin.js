import {AdminLayouts } from "../layouts"
import {loginAdmin} from "../pages/Admin"

const rutasAdimn =[
    {
        path: "/admin",
        layout: AdminLayouts,
        component: loginAdmin
    }
]

export default rutasAdimn;

from django.contrib import admin
from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Restaurante  API Doc",
        default_version='v1',
        description="Documentacion de la API de Restaurante.",
        terms_of_service="",
        contact=openapi.Contact(email="andymrrrr@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True
)
urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', schema_view.with_ui('swagger',
         cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc',
         cache_timeout=0), name='schema-redoc'),
]
